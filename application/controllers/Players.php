<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Players controller
 */

class Players extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//load the club model
		$this->load->model('players_model');
		$this->load->model('clubs_model'); //needed for function get_available_clubs()
	}

	public function create()
	{
		$aData['aPositions'] = $this->players_model->get_positions();
		$aData['aAvailableClubs'] = $this->clubs_model->get_available_clubs();
		$this->load->view('templates/header');
		$this->load->view('players/create', $aData);
		$this->load->view('templates/footer');
	}

	public function save_new()
	{
		$this->players_model->create_player();
	}

	public function deactivate($iPlayerID)
	{
		$aData = $this->players_model->deactivate_player($iPlayerID);
		Players::index($aData);
	}

	public function index($aData = array())
	{
		$aData['aUnassignedPlayers'] = $this->players_model->get_unassigned_players();
		$aData['aAssignedPlayers'] = $this->players_model->get_assigned_players();
		$this->load->view('templates/header');
		$this->load->view('players/list', $aData);
		$this->load->view('templates/footer');
	}

	public function edit($iPlayerID)
	{
		$aData = $this->players_model->get_player($iPlayerID);
		$aData['aAvailableClubs'] = $this->clubs_model->get_available_clubs();

		$this->load->view('templates/header');
		$this->load->view('players/edit', $aData);
		$this->load->view('templates/footer');
	}

	public function update($iPlayerID)
	{
		$this->players_model->update_player($iPlayerID);
	}

}