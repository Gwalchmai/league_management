<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Default home controller
 */

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//load the club model
		$this->load->model('clubs_model');

	}

	public function index()
	{
		$aData['aClubs'] = $this->clubs_model->get_club();

		$this->load->view('templates/header');
		$this->load->view('home/home', $aData);
		$this->load->view('templates/footer');

	}
}