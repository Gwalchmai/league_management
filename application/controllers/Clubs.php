<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Clubs controller
 */

class Clubs extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//load the club model
		$this->load->model('clubs_model');
		$this->load->model('players_model'); //needed for function get_players_in_club();
	}

	public function update($iClubID)
	{
		$this->clubs_model->update_club($iClubID);
	}

	public function save_new()
	{
		$this->clubs_model->create_club();
	}

	public function edit($iClubID = 0)
	{
		$aData['aClub'] = $this->clubs_model->get_club($iClubID);
		$this->load->view('templates/header');
		$this->load->view('clubs/edit', $aData);
		$this->load->view('templates/footer');
	}

	public function create()
	{
		$this->load->view('templates/header');
		$this->load->view('clubs/create');
		$this->load->view('templates/footer');
	}

	public function deactivate($iClubID)
	{
		$aData = $this->clubs_model->deactivate_club($iClubID);
		Clubs::index();
	}

	public function index()
	{
		$aData['aClubs'] = $this->clubs_model->get_club();

		$this->load->view('templates/header');
		$this->load->view('clubs/list', $aData);
		$this->load->view('templates/footer');
	}
}