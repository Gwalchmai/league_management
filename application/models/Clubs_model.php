<?php

/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Clubs Model
 */
class Clubs_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_club($iClubID = FALSE)
	{
		if($iClubID === FALSE) {
			$query = $this->db->get_where('clubs', array('active' => 1));
			$aData = $query->result_array();
			foreach($aData as $iKey => $aClub) {
				$this->db->join('positions', 'positions.id = players.position_id');
				$query = $this->db->get_where('players', array('club_id' => $aClub['id']));
				$aData[$iKey]['aPlayers'] = $query->result_array();
			}
			return $aData;
		}

		$query = $this->db->get_where('clubs', array('id' => $iClubID));
		return $query->row_array();
	}

	public function update_club($iClubID)
	{
		//update the logo if there is a change
		$aUploadConfig['upload_path'] = './assets/images/';
		$aUploadConfig['allowed_types'] = 'gif|jpg|png';
		$aUploadConfig['overwrite'] = true;
		$aUploadConfig['remove_spaces'] = true;
		$this->load->library('upload', $aUploadConfig);
		$sFieldName = 'inputLogoFile';
		$iDateFounded = strtotime($this->input->post('inputDateFounded'));
		if($this->upload->do_upload($sFieldName)) {
			$aUploadData = $this->upload->data();
			$aClubData = array('id' => $iClubID,
			                   'club_name' => $this->input->post('inputClub'),
			                   'city' => $this->input->post('inputCity'),
			                   'coach' => $this->input->post('inputCoach'),
			                   'date_founded' => $iDateFounded,
			                   'active' => $this->input->post('inputActive'),
			                   'logo_filename' => $aUploadData['file_name']);
		} else {
			$aClubData = array('id' => $iClubID,
			                   'club_name' => $this->input->post('inputClub'),
			                   'city' => $this->input->post('inputCity'),
			                   'coach' => $this->input->post('inputCoach'),
			                   'date_founded' => $iDateFounded,
			                   'active' => $this->input->post('inputActive'),);
		}

		$iResult = $this->db->update('clubs', $aClubData);
		if($iResult == true || $iResult == 1) echo 'Success'; else {
			$sErrors = "File errors: <br> $this->upload->display_errors() <br> ";
			echo $sErrors;
		}
	}

	public function create_club()
	{
		$aUploadConfig['upload_path'] = './assets/images/';
		$aUploadConfig['allowed_types'] = 'gif|jpg|png';
		$aUploadConfig['overwrite'] = true;
		$aUploadConfig['remove_spaces'] = true;
		$this->load->library('upload', $aUploadConfig);
		$sFieldName = 'inputLogoFile';
		$iDateFounded = strtotime($this->input->post('inputDateFounded'));
		if($this->upload->do_upload($sFieldName)) {
			$aUploadData = $this->upload->data();
			$aClubData = array('club_name' => $this->input->post('inputClub'),
			                   'city' => $this->input->post('inputCity'),
			                   'coach' => $this->input->post('inputCoach'),
			                   'date_founded' => $iDateFounded,
			                   'active' => $this->input->post('inputActive'),
			                   'logo_filename' => $aUploadData['file_name']);
		}

		$iResult = $this->db->insert('clubs', $aClubData);
		if($iResult == true || $iResult == 1) echo 'Success'; else {
			$sErrors = "File errors: <br> $this->upload->display_errors() <br> ";
			echo $sErrors;
		}
	}

	public function deactivate_club($iClubID)
	{
		$aClubData = array('id' => $iClubID,
		                   'active' => 0);
		$aResult['iClubsDeactivated'] = $this->db->update('clubs', $aClubData);
		$this->db->where('club_id', $iClubID);
		$aPlayerData = array('club_id' => 0);
		$aResult['iPlayersMadeAvailable'] = $this->db->update('players', $aPlayerData);
		return $aResult;
	}

	public function get_available_clubs()
	{
		$this->db->where('active = ', 1);
		$this->db->where('player_count < ', 16);
		$query = $this->db->get('clubs');
		return $query->result_array();
	}
}