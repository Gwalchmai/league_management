<?php

/**
 * Created by: Dylan Moss
 * Date: 29/08/2017
 * Description: Players Model
 */
class Players_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_player($iPlayerID = FALSE)
	{
		if($iPlayerID === FALSE) {
			$this->db->join('positions', 'positions.id = players.position_id');
			$query = $this->db->get_where('players', array('active' => 1));
			return $query->result_array();
		} else {
			$this->db->join('positions', 'positions.id = players.position_id');
			$query = $this->db->get_where('players', array('players.id' => $iPlayerID));
			$aData['aPlayer'] = $query->row_array();

			$query = $this->db->get('positions');
			$aData['aPositions'] = $query->result_array();

			return $aData;
		}
	}

	public function get_unassigned_players()
	{
		$this->db->join('positions', 'positions.id = players.position_id');
		$query = $this->db->get_where('players', array('players.active' => 1,
		                                               'club_id' => 0));
		return $query->result_array();
	}

	public function get_assigned_players()
	{
		$this->db->join('positions', 'positions.id = players.position_id');
		$query = $this->db->get_where('players', array('players.active' => 1,
		                                               'club_id != ' => 0));
		return $query->result_array();
	}

	public function get_positions()
	{
		$query = $this->db->get('positions');
		return $query->result_array();
	}

	public function create_player()
	{
		$iBirthdate = strtotime($this->input->post('inputBirthdate'));
		$aPlayerData = array('name' => $this->input->post('inputName'),
		                     'position_id' => $this->input->post('inputPosition'),
		                     'club_id' => $this->input->post('inputClub'),
		                     'jersey_number' => $this->input->post('inputJerseyNumber'),
		                     'birthdate' => $iBirthdate,
		                     'active' => $this->input->post('inputActive'),);

		$iResult = $this->db->insert('players', $aPlayerData);
		if($iResult == true || $iResult == 1) echo 'Success'; else echo 'Failure';
	}

	public function deactivate_player($iPlayerID)
	{
		$aPlayerData = array('id' => $iPlayerID,
		                     'active' => 0);
		$this->db->update('players', $aPlayerData);
	}

	public function update_player($iPlayerID)
	{
		$iBirthdate = strtotime($this->input->post('inputBirthdate'));
		$aPlayer = array('id' => $iPlayerID,
		                 'name' => $this->input->post('inputName'),
		                 'jersey_number' => $this->input->post('inputJerseyNumber'),
		                 'position_id' => $this->input->post('inputPosition'),
		                 'club_id' => $this->input->post('inputClub'),
		                 'birthdate' => $iBirthdate,
		                 'active' => $this->input->post('inputActive'));
		$iResult = $this->db->update('players', $aPlayer);
		if($iResult == true || $iResult == 1) echo 'Success'; else echo 'Failure';
	}

	public function get_players_in_club($iClubID)
	{
		$query = $this->db->get_where('players', array('club_id' => $iClubID));
		return $query->result_array();
	}
}