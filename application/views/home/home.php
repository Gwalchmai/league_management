<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Home page body
 */
?>
<div class="container">
	<table class="table table-striped table-hover">
		<tr>

			<th></th>
			<th>Club</th>
			<th>City</th>
			<th>Coach</th>
			<th>Date Founded</th>
		</tr>
		<?php if(isset($aClubs) && is_array($aClubs)) {
			foreach($aClubs as $iKey => $aClub):
				$sDateFounded = date("Y-m-d", $aClub['date_founded']); ?>
				<tr>
					<td><img src=" <?= base_url() ?>assets/images/<?= $aClub['logo_filename'] ?>" height="100px"></td>
					<td><?= $aClub['club_name'] ?></td>
					<td><?= $aClub['city'] ?></td>
					<td><?= $aClub['coach']; ?></td>
					<td><?= $sDateFounded ?></td>
				</tr>
			<?php endforeach;
		} else { ?>
			<tr>
				<td colspan="6">No Clubs Loaded</td>
			</tr>
		<?php }; ?>
	</table>
</div>
