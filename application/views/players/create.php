<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Create an unassigned player
 */

//create the options for positions string
$sPositionOptions = '';
foreach($aPositions as $iKey => $aPosition) {
	$sPositionOptions .= "<option value = \"$aPosition[id]\">$aPosition[position_name]</option>";
}

//create the options for available teams string
$sClubsOptions = '';
foreach($aAvailableClubs as $iKey => $aClub) {
	$sClubsOptions .= "<option value=\"$aClub[id]\">$aClub[club_name]</option>";
}
?>
<div class="container">
	<form class="form-horizontal" name="playerForm" id="playerForm" method="post" enctype="multipart/form-data">
		<input type="hidden" name="baseURL" id="baseURL" value="<?php echo base_url(); ?>">
		<input type="hidden" name="inputID" id="inputID" value="0">
		<fieldset>
			<legend>New Player</legend>

			<div class="form-group">
				<label for="inputName" class="col-md-2 control-label">Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputName" id="inputName" placeholder="Player Name" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputJerseyNumber" class="col-md-2 control-label">Jersey Number</label>
				<div class="col-md-10">
					<input class="form-control" name="inputJerseyNumber" id="inputJerseyNumber" placeholder="Jersey Number" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputBirthdate" class="col-md-2 control-label">Birthdate</label>
				<div class="col-md-10">
					<input class="form-control" name="inputBirthdate" id="inputBirthdate" placeholder="Birthdate" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputPosition" class="col-md-2 control-label">Position</label>
				<div class="col-md-10">
					<select class="form-control" name="inputPosition" id="inputPosition"><?= $sPositionOptions ?></select>
				</div>
			</div>

			<div class="form-group">
				<label for="inputClub" class="col-md-2 control-label">Club</label>
				<div class="col-md-10">
					<select class="form-control" name="inputClub" id="inputClub"><?= $sClubsOptions ?></select>
				</div>
			</div>

			<div class="form-group">
				<label for="inputActive" class="col-lg-2 control-label">Active</label>
				<div class="col-md-10">
					<input class="form-control" name="inputActive" id="inputActive" type="checkbox" value="1" checked>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="reset" class="btn btn-default">Cancel</button>
					<button type="submit" name="submit" id="submit_btn" value="send" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script src='<?php echo base_url(); ?>assets/js/validate_player.js'></script>