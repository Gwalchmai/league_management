<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: list unassigned players
 */
?>
<div class="container">
	<legend>Unassigned Players</legend>
	<table class="table table-striped table-hover">
		<tr>
			<th>Name</th>
			<th>Position</th>
			<th>Jersey Number</th>
			<th>Birthdate</th>
			<th></th>
		</tr>
		<?php if(isset($aUnassignedPlayers) && is_array($aUnassignedPlayers)) {
			foreach($aUnassignedPlayers as $iKey => $aPlayer):
				$sBirthdate = date("Y-m-d", $aPlayer['birthdate']);
				?>
				<tr>
					<td><?= $aPlayer['name'] ?></td>
					<td><?= $aPlayer['position_name'] ?></td>
					<td><?= $aPlayer['jersey_number'] ?></td>
					<td><?= $sBirthdate ?></td>
					<td>
						<a href="<?php echo base_url(); ?>players/manage/<?= $aPlayer['id'] ?>" class="btn btn-info btn-sm">Edit</a>
						<div class="btn btn-danger btn-sm" data-toggle="modal" data-target="#Deactivate">Deactivate</div>
						<div id="Deactivate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Confirm Deactivate Player</h4>
									</div>
									<div class="modal-body">
										<p>Please confirm the player deactivation. The Player will not be available for assignment after this action.</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary" onclick="window.location.href='players/deactivate/<?= $aPlayer['id'] ?>'">Deactivate Player</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			<?php endforeach;
		} else { ?>
			<tr>
				<td colspan="6">No Unassigned Players Available</td>
			</tr>
		<?php }; ?>
		<tr>
			<td colspan="6" style="text-align: right;"><a href="<?php echo base_url(); ?>players/create" class="btn btn-primary btn-sm">Create</a></td>
		</tr>
	</table>
</div>

<div class="container">
	<legend>Assigned Players</legend>
	<table class="table table-striped table-hover">
		<tr>
			<th>Name</th>
			<th>Position</th>
			<th>Jersey Number</th>
			<th>Birthdate</th>
			<th></th>
		</tr>
		<?php if(isset($aAssignedPlayers) && is_array($aAssignedPlayers)) {
			foreach($aAssignedPlayers as $iKey => $aPlayer):
				$sBirthdate = date("Y-m-d", $aPlayer['birthdate']);
				?>
				<tr>
					<td><?= $aPlayer['name'] ?></td>
					<td><?= $aPlayer['position_name'] ?></td>
					<td><?= $aPlayer['jersey_number'] ?></td>
					<td><?= $sBirthdate ?></td>
					<td>
						<a href="<?php echo base_url(); ?>players/manage/<?= $aPlayer['id'] ?>" class="btn btn-info btn-sm">Edit</a>
						<div class="btn btn-danger btn-sm" data-toggle="modal" data-target="#Deactivate">Deactivate</div>
						<div id="Deactivate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Confirm Deactivate Player</h4>
									</div>
									<div class="modal-body">
										<p>Please confirm the player deactivation. The Player will not be available for assignment after this action.</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary" onclick="window.location.href='players/deactivate/<?= $aPlayer['id'] ?>'">Deactivate Player</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			<?php endforeach;
		} else { ?>
			<tr>
				<td colspan="6">No Assigned Players Available</td>
			</tr>
		<?php }; ?>
		<tr>
			<td colspan="6" style="text-align: right;"><a href="<?php echo base_url(); ?>players/create" class="btn btn-primary btn-sm">Create</a></td>
		</tr>
	</table>
</div>