<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Footer view template
 */
?>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
<script src='<?php echo base_url(); ?>assets/js/npm.js'></script>
<script src='<?php echo base_url(); ?>assets/js/bootstrap.js'></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script>
    $(document).ready(function () {
        var date_input = $('input[name="inputDateFounded"]');
        var container = $('#clubForm form').length > 0 ? $('#clubForm').parent() : "body";
        var options = {
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>

<script>
    $(document).ready(function () {
        var date_input = $('input[name="inputBirthdate"]');
        var container = $('#playerForm form').length > 0 ? $('#playerForm').parent() : "body";
        var options = {
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>

</body>
</html>


