<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Header view template
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo link_tag('assets/css/styles.css'); ?>
	<title>League Management</title>
</head>
<body>

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url(); ?>">League Management</a>
		</div>
		<ul class="nav nav-tabs">
			<li class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
			<li><a href="<?php echo base_url(); ?>clubs">Manage Clubs</a></li>
			<li><a href="<?php echo base_url(); ?>players">Manage Players</a></li>
		</ul>
	</div>
</nav>