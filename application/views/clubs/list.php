<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Show Details of a club for management
 */
?>
<div class="container">
	<table class="table table-striped table-hover">
		<tr>
			<th></th>
			<th>Club</th>
			<th>City</th>
			<th>Coach</th>
			<th>Date Founded</th>
			<th></th>
		</tr>
		<?php if(isset($iClubsDeactivated) && isset($iPlayersMadeAvailable)) echo "<tr><td colspan=\"6\" class=\"text-success\">$iClubsDeactivated Clubs Deactivated, $iPlayersMadeAvailable Players Made Available.</td></tr>" ?>
		<?php if(isset($aClubs) && is_array($aClubs)) {
			foreach($aClubs as $iKey => $aClub):
				$sDateFounded = date("Y-m-d", $aClub['date_founded']);
			?>
					<tr>
					<td><img src=" <?= base_url() ?>assets/images/<?= $aClub['logo_filename'] ?>" height="100px"></td>
					<td><?= $aClub['club_name'] ?></td>
					<td><?= $aClub['city'] ?></td>
					<td><?= $aClub['coach'] ?></td>
					<td><?= $sDateFounded ?></td>
					<td>
						<button class="btn btn-info btn-sm" data-toggle="collapse" data-target="#club_<?= $aClub['id'] ?>">Players</button>
						<a href="<?php echo base_url(); ?>clubs/manage/<?= $aClub['id'] ?>" class="btn btn-info btn-sm">Edit</a>
						<div class="btn btn-danger btn-sm" data-toggle="modal" data-target="#Deactivate">Deactivate</div>
						<div id="Deactivate" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title">Confirm Deactivate Team</h4>
									</div>
									<div class="modal-body">
										<p>Please confirm the team deactivation. All team members will become available for re-assignment after this action.</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-sm btn-primary" onclick="window.location.href='clubs/deactivate/<?= $aClub['id'] ?>'">Deactivate Team</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
				</div>
				<tr>
				<td colspan="6">
				<div id="club_<?= $aClub['id'] ?>" class="collapse">
					<?php if (isset($aClub['aPlayers']) && is_array($aClub['aPlayers'])){
						?>
						<table class="table table-striped table-hover">
							<tr>
								<th>Player Name</th>
								<th>Position</th>
								<th>Jersey Number</th>
								<th>Birthdate</th>
							</tr>
							<?php foreach ($aClub['aPlayers'] as $iKey => $aPlayer){
								$sBirthdate = date("Y-m-d", $aPlayer['birthdate']) ?>
								<tr>
									<td><?= $aPlayer['name'] ?></td>
									<td><?= $aPlayer['position_name'] ?></td>
									<td><?= $aPlayer['jersey_number'] ?></td>
									<td><?= $sBirthdate ?></td>
								</tr>
							<?php } ?>
						</table>
						</div>
					<?php } ?>
				</td>
				</tr>
			<?php endforeach;
		} else { ?>
			<tr>
				<td colspan="6">No Clubs Loaded</td>
			</tr>
		<?php }; ?>
		<tr>
			<td colspan="6" style="text-align: right;"><a href="<?php echo base_url(); ?>clubs/create" class="btn btn-primary btn-sm">Create</a></td>
		</tr>
	</table>

</div>
