<?php
/**
 * Created by: Dylan Moss
 * Date: 27/08/2017
 * Description: Edit Club
 */
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<div class="container">
	<form class="form-horizontal" name="clubForm" id="clubForm" method="post" enctype="multipart/form-data">
		<input type="hidden" name="baseURL" id="baseURL" value="<?php echo base_url(); ?>">
		<input type="hidden" name="inputID" id="inputID" value="0">
		<fieldset>
			<legend>New Club</legend>

			<div class="form-group">
				<label for="inputClub" class="col-md-2 control-label">Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputClub" id="inputClub" placeholder="Club Name" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputCity" class="col-md-2 control-label">City</label>
				<div class="col-md-10">
					<input class="form-control" name="inputCity" id="inputCity" placeholder="Club City" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputCoach" class="col-md-2 control-label">Coach Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputCoach" id="inputCoach" placeholder="Coach Name" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputDateFounded" class="col-md-2 control-label">Date Founded</label>
				<div class="col-md-10">
					<input class="form-control" name="inputDateFounded" id="inputDateFounded" placeholder="Club Date Founded" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputLogoFile" class="col-md-2 control-label">Logo File</label>
				<div class="col-lg-10">
					<input class="form-control" type="file" id="inputLogoFile" name="inputLogoFile" accept="image/*" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputActive" class="col-lg-2 control-label">Active</label>
				<div class="col-md-10">
					<input class="form-control" name="inputActive" id="inputActive" type="checkbox" value="1" checked>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="reset" class="btn btn-default">Cancel</button>
					<button type="submit" name="submit" id="submit_btn" value="send" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script src='<?php echo base_url(); ?>assets/js/validate_club.js'></script>