<?php
/**
 * Created by: Dylan Moss
 * Date: 25/08/2017
 * Description: Edit Club
 */;
$sActiveChecked = $aClub['active'] == 1 ? 'checked' : '';
$sDateFounded = date("Y-m-d", $aClub['date_founded']);
$iClubID = filter_var($aClub['id'], FILTER_SANITIZE_NUMBER_INT);
?>
<div class="container">
	<form class="form-horizontal" name="clubForm" id="clubForm" method="post" enctype="multipart/form-data">
		<input type="hidden" name="inputID" id="inputID" value="<?= $iClubID ?>">
		<input type="hidden" name="baseURL" id="baseURL" value="<?php echo base_url(); ?>">
		<fieldset>
			<legend>Edit Club</legend>

			<div class="form-group">
				<label for="inputClub" class="col-md-2 control-label">Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputClub" id="inputClub" placeholder="Club Name" value="<?= $aClub['club_name'] ?>" type="text" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputCity" class="col-md-2 control-label">City</label>
				<div class="col-md-10">
					<input class="form-control" name="inputCity" id="inputCity" placeholder="Club City" value="<?= $aClub['city'] ?>" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputCoach" class="col-md-2 control-label">Coach Name</label>
				<div class="col-md-10">
					<input class="form-control" name="inputCoach" id="inputCoach" placeholder="Coach Name" value="<?= $aClub['coach'] ?>" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputDateFounded" class="col-md-2 control-label">Date Founded</label>
				<div class="col-md-10">
					<input class="form-control" name="inputDateFounded" id="inputDateFounded" placeholder="Club Date Founded" value="<?= $sDateFounded ?>" required>
				</div>
			</div>

			<div class="form-group">
				<label for="inputLogoFile" class="col-md-2 control-label">Logo File</label>
				<div class="col-lg-10">
					<input class="form-control" type="file" id="inputLogoFile" name="inputLogoFile" accept="image/*">
				</div>
			</div>

			<div class="form-group">
				<label for="inputActive" class="col-lg-2 control-label">Active</label>
				<div class="col-md-10">
					<input class="form-control" name="inputActive" id="inputActive" type="checkbox" value="1" <?= $sActiveChecked ?>>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-10 col-md-offset-2">
					<button type="reset" class="btn btn-default">Cancel</button>
					<button type="submit" name="submit" id="submit_btn" value="send" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>
<script src='<?php echo base_url(); ?>assets/js/validate_club.js'></script>