//get the variables from the club form
var playerForm = document.getElementById('playerForm');
var inputName = document.getElementById('inputName').value;
var inputJerseyNumber = document.getElementById('inputJerseyNumber').value;
var inputBirthdate = document.getElementById('inputBirthdate').value;
var inputPosition = document.getElementById('inputPosition').value;
var inputClub = document.getElementById('inputClub').value;
var inputActive = document.getElementById('inputActive').value;
var inputID = document.getElementById('inputID').value;
var baseURL = document.getElementById('baseURL').value;
var submit_btn = document.getElementById('submit_btn');

//define the function to validate and send the data
playerForm.onsubmit = function (event) {
    event.preventDefault();
    // Update button text.
    submit_btn.innerHTML = 'Updating Player';

    //now create a formData variable and append everything to it
    var playerData = new FormData(playerForm);
    playerData.append('player_name', inputName);
    playerData.append('jersey_number', inputJerseyNumber);
    playerData.append('birthdate', inputBirthdate);
    playerData.append('position', inputPosition);
    playerData.append('club_id', inputClub);
    playerData.append('active', inputActive);
    playerData.append('id', inputID);

    // now send the update to the server
    var request = new XMLHttpRequest();
    if (inputID != 0) var updateURL = baseURL + 'players/update/' + inputID;
    else var updateURL = baseURL + '/players/save_new';

    request.open('POST', updateURL, true);

    request.send(playerData);
    submit_btn.innerHTML = 'Updated Player';
};