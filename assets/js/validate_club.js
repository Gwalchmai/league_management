//get the variables from the club form
var clubForm = document.getElementById('clubForm');
var inputClub = document.getElementById('inputClub').value;
var inputCity = document.getElementById('inputCity').value;
var inputCoach = document.getElementById('inputCoach').value;
var inputDateFounded = document.getElementById('inputDateFounded').value;
var inputActive = document.getElementById('inputActive').value;
var inputID = document.getElementById('inputID').value;
var baseURL = document.getElementById('baseURL').value;
var submit_btn = document.getElementById('submit_btn');
var inputLogoFile = document.getElementById('inputLogoFile');

//define the function to validate and send the data
clubForm.onsubmit = function (event) {
    event.preventDefault();
    // Update button text.
    submit_btn.innerHTML = 'Updating Club';

    //now create a formData variable and append everything to it
    var clubData = new FormData(clubForm);
    clubData.append('club_name', inputClub);
    clubData.append('city', inputCity);
    clubData.append('coach', inputCoach);
    clubData.append('date_founded', inputDateFounded);
    clubData.append('active', inputActive);
    clubData.append('id', inputID);

    // now send the update to the server
    var request = new XMLHttpRequest();
    if (inputID !=0)  var updateURL = baseURL + 'clubs/update/' + inputID;
    else var updateURL = baseURL + '/clubs/save_new';

    request.open('POST', updateURL, true);

    request.send(clubData);
    submit_btn.innerHTML = 'Updated Club';
};